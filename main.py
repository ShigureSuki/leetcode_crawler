#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
import logging
import os
import random
import re
import sqlite3
import threading
import time

import html2text
import requests
from openpyxl import Workbook
from requests_toolbelt import MultipartEncoder

from mytools import *

db_path = 'leetcode.db'
user_agent = r'Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0'
max_thread_num = 12
threadLock = threading.Lock()


def initLock(l):
    global lock
    lock = l


class DatabaseWriterThread(threading.Thread):
    """
    线程类，获取题目信息
    """

    def __init__(self, _slug, _status, _crawler):
        threading.Thread.__init__(self)
        self.title_slug = _slug
        self.status = _status
        self.crawler = _crawler

    def run(self):
        success = False
        conn = sqlite3.connect(db_path, timeout=10)
        while not success:
            try:
                # sleep for 1~3 seconds randomly in order not to be banned by server
                time.sleep(random.randint(1, 3))

                cursor = conn.cursor()
                session = requests.Session()
                headers = {'User-Agent': user_agent, 'Connection': 'keep-alive',
                           'Content-Type': 'application/json',
                           'Referer': 'https://leetcode.com/problems/' + self.title_slug}

                url = "https://leetcode.com/graphql"
                params = {'operationName': "getQuestionDetail",
                          'variables': {'titleSlug': self.title_slug},
                          'query': '''query getQuestionDetail($titleSlug: String!) {
                        question(titleSlug: $titleSlug) {
                            questionId
                            questionFrontendId
                            questionTitle
                            questionTitleSlug
                            content
                            difficulty
                            stats
                            similarQuestions
                            categoryTitle
                            topicTags {
                            name
                            slug
                        }
                    }
                }'''
                          }

                json_data = json.dumps(params).encode('utf8')

                content = None
                try:
                    resp = session.post(url, data=json_data,
                                        headers=headers, timeout=10)
                    content = resp.json()
                except json.decoder.JSONDecodeError:
                    logging.warning(
                        f"Grasping of the problem {self.title_slug} failed. Try again later.")
                    crawler.failcnt += 1
                    continue
                pid = content['data']['question']['questionId']
                tags = [tag['name']
                        for tag in content['data']['question']['topicTags']]

                if content['data']['question']['content'] is not None:
                    pdetail = (pid,
                               content['data']['question']['questionFrontendId'],
                               content['data']['question']['questionTitle'],
                               content['data']['question']['questionTitleSlug'],
                               content['data']['question']['difficulty'],
                               content['data']['question']['content'],
                               self.status)

                    threadLock.acquire()
                    cursor.execute(
                        'INSERT INTO problems (id, frontend_id, title, slug, difficulty, content, status) VALUES (?, ?, ?, ?, ?, ?, ?)',
                        pdetail)
                    for tag in tags:
                        problem_tags = (pid, tag)
                        cursor.execute(
                            'INSERT INTO problem_tags (problem_id, tag) VALUES (?, ?)', problem_tags)
                    conn.commit()
                    logging.info(f"Insert problem [{self.title_slug}] success")
                    crawler.optcnt += 1
                    threadLock.release()
                    success = True
            # 若出现连接超时或连接错误则继续获取
            except (requests.exceptions.Timeout, requests.exceptions.ConnectionError) as error:
                print(str(error))
        cursor.close()
        conn.close()


class LeetcodeCrawler:
    def __init__(self):
        self.session = requests.Session()
        self.csrftoken = ''
        self.parameters = {}
        self.is_login = False
        self.conn = None  # 爬虫相关的数据库连接
        self.optcnt = 0
        self.failcnt = 0
    """
    根据获得的参数初始化爬虫。如果出现非法参数，中止初始化并抛出异常。
    """

    def init(self, args_dict):
        if args_dict.get('verbose'):
            logging.basicConfig(level=logging.DEBUG)
        else:
            logging.basicConfig(level=logging.INFO)
        if args_dict.get('directory'):
            self.parameters['directory'] = args_dict['directory']
            logging.debug(
                f"Specified output directory = {self.parameters['directory']}")

        self.get_csrftoken()

        if args_dict.get('code') or args_dict.get('stat'):
            if not (args_dict.get('name') and args_dict.get('passwd')):
                raise ValueError("It is necessary to provide username and password when fetching status "
                                 "or submission code")
            if not self.login(args_dict['name'], args_dict['passwd']):
                raise ValueError("Login failure. Check username and password")
            self.parameters['login'] = True
            if args_dict.get('code'):
                self.parameters['path'] = args_dict['code']
                logging.debug(
                    f"Specified code path = {self.parameters['path']}")
            if args_dict.get('stat'):
                self.parameters['status'] = args_dict['stat']
                logging.debug(f"Specified status range = {self.parameters['status']}")
        if args_dict.get('lv') or argsDict.get('tags'):
            if args_dict.get('lv'):
                self.parameters["difficulty"] = args_dict['lv']
                logging.debug(f"Specified difficulty range = {self.parameters['difficulty']}")
            if args_dict.get('tags'):
                self.parameters["tags"] = args_dict['tags']
                logging.debug(f"Specified tag = {self.parameters['tags']}")

    """
    针对反爬虫的措施：获取token。
    """

    def get_csrftoken(self):
        url = 'https://leetcode.com'
        cookies = self.session.get(url).cookies
        for cookie in cookies:
            if cookie.name == 'csrftoken':
                self.csrftoken = cookie.value
                break

    def login(self, username, password):
        """
        使用requests库获取登陆leetcode的session。
        """
        url = "https://leetcode.com/accounts/login"

        me = MultipartEncoder({
            'csrfmiddlewaretoken': self.csrftoken,
            'login': username,
            'password': password,
            'next': 'problems'
        })

        headers = {'User-Agent': user_agent, 'Connection': 'keep-alive',
                   'Referer': 'https://leetcode.com/accounts/login/', 'origin': "https://leetcode.com",
                   'Content-Type': me.content_type}

        self.session.post(url, headers=headers, data=me,
                          timeout=10, allow_redirects=False)
        logging.debug(
            f"cookies: {self.session.cookies.get('LEETCODE_SESSION')}")
        self.is_login = self.session.cookies.get(
            'LEETCODE_SESSION') is not None
        if self.is_login:
            logging.info("Login Successful")
        return self.is_login

    def get_problems(self):
        url = "https://leetcode.com/api/problems/all/"

        headers = {'User-Agent': user_agent, 'Connection': 'keep-alive'}
        resp = self.session.get(url, headers=headers, timeout=10)

        problems_list = json.loads(resp.content.decode('utf-8'))

        prob_upd_list = []
        threads = []

        cursor = self.conn.cursor()

        difficulty_dict = {1: "Easy", 2: "Medium", 3: "Hard"}

        for problem in problems_list['stat_status_pairs']:
            pid = problem['stat']['question_id']
            pslug = problem['stat']['question__title_slug']
            pstat = problem['status']

            pdiff = difficulty_dict.get(problem['difficulty']['level'], "None")

            if self.parameters.get('difficulty') and pdiff not in self.parameters['difficulty']:
                continue

            if self.parameters.get('status') and pstat not in self.parameters['status']:
                continue

            if problem['paid_only']:
                continue

            logging.debug(
                f"Trying to fetch, ID:{pid}, SLUG:{pslug}, STAT:{pstat}, DIFF:{pdiff}")
            cursor.execute('SELECT status FROM problems WHERE id = ?', (pid,))
            result = cursor.fetchone()
            if not result:
                # 创建新线程
                thread = DatabaseWriterThread(pslug, pstat, self)
                thread.start()
                while True:
                    # 判断正在运行的线程数量,如果小于5则退出while循环,
                    # 进入for循环启动新的进程.否则就一直在while循环进入死循环
                    if len(threading.enumerate()) < max_thread_num:
                        break
                # 添加线程到线程列表
                threads.append(thread)
            elif self.is_login and pstat != result[0]:
                prob_upd_list.append((pstat, pid))
        for t in threads:
            t.join()

        cursor.executemany(
            'UPDATE problems SET status = ? WHERE id = ?', prob_upd_list)
        self.conn.commit()
        cursor.close()
        logging.info(f"{self.optcnt} problems downloaded, and {self.failcnt} operations failed.")

    def connect_db(self, path):
        self.conn = sqlite3.connect(path, timeout=10)
        cursor = self.conn.cursor()

        # Table 'problems'
        cursor.execute(
            "SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name = 'problems';")
        if cursor.fetchone()[0] == 0:
            cursor.execute('''CREATE TABLE problems
                    (id                INT      PRIMARY KEY     NOT NULL,
                    frontend_id        INT                      NOT NULL,
                    title              CHAR(50)                 NOT NULL,
                    slug               CHAR(50)                 NOT NULL,
                    difficulty         CHAR(10)                 NOT NULL,
                    content            TEXT                     NOT NULL,
                    status             CHAR(10));''')

        # Table 'last_ac_submission_record'
        cursor.execute(
            "SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name = 'last_ac_submission_record';")
        if cursor.fetchone()[0] == 0:
            cursor.execute('''CREATE TABLE last_ac_submission_record
                    (id      INT PRIMARY KEY       NOT NULL,
                    problem_slug      CHAR(50)    NOT NULL,
                    timestamp          INT         NOT NULL,
                    language         CHAR(10)      NOT NULL,
                    code               TEXT,
                    runtime            CHAR(10));''')

        # Table 'problem_tags'
        cursor.execute(
            "SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name = 'problem_tags';")
        if cursor.fetchone()[0] == 0:
            cursor.execute('''CREATE TABLE problem_tags
                    (problem_id      INT       NOT NULL,
                    tag      CHAR(30)    NOT NULL);''')

        cursor.close()
        
    def generate_problems_excel(self):
        """
        生成本次操作获得的所有题目的信息的excel文档。
        """

        # 从数据库提取数据
        cursor = self.conn.cursor()
        cursor.execute("SELECT id,frontend_id, title, slug, difficulty, status FROM problems;")
        DataLists = cursor.fetchall()

        if DataLists is None:
            logging.warning("Fail to generate excel: the database is empty.")
            return

        now_wb = Workbook()
        active_sheet = now_wb.create_sheet(f"{time.strftime('%y-%m-%d', time.localtime())}")


        names = ["id", "frontend_id", "tags", "title", "slug", "difficulty"]
        # 设置标题
        for i, name in enumerate(names):
            active_sheet.cell(row=1, column=i+1, value=name.capitalize())

        cnt = 2
        for i, data_list in enumerate(DataLists):
            cursor.execute('SELECT tag From problem_tags WHERE problem_id = ?', (data_list[0],))
            tags = cursor.fetchall()

            p_detail = {
                'id': data_list[0],
                'frontedId': data_list[1],
                'title': data_list[2],
                'slug': data_list[3],
                'difficulty': data_list[4],
                'status': data_list[5],
                'tags': tags
            }

            logging.debug(f"trying to insert {data_list}, tags = {tags}")
            if not self.filter_problems(p_detail):
                continue
            logging.debug(f"excel insert success")
            tag_str = tuple_into_str(tags)

            data_list = list(data_list)
            data_list.insert(2, tag_str)
            data_list = data_list[:-1]

            for j, data in enumerate(data_list):
                active_sheet.cell(row=cnt, column=j+1, value=data)
            cnt += 1

        now_wb.save("statistic.xlsx")
        logging.info("Excel document generated.")

    def generate_problems_markdown(self):
        if not os.path.isdir(self.parameters['directory']):
            os.mkdir(self.parameters['directory'])

        cursor = self.conn.cursor()
        cursor.execute("SELECT * FROM problems")
        for row in cursor:
            p_detail = {
                'id': row[0],
                'frontedId': row[1],
                'title': row[2],
                'slug': row[3],
                'difficulty': row[4],
                'content': row[5],
                'status': row[6]
            }

            if not self.filter_problems(p_detail):
                continue

            tags = ''
            tag_cursor = self.conn.cursor()
            tag_cursor.execute('SELECT tag FROM problem_tags WHERE problem_id = ?', (row[0],))
            tag_list = tag_cursor.fetchall()

            for tag in tag_list:
                tags += tag[0] + ', '

            if len(tags) > 2:
                tags = tags[:-2]
            p_detail['tags'] = tags
            self.generate_problem_markdown(p_detail, self.parameters['directory'], self.parameters.get('code'))

        cursor.close()
        logging.info("Markdown documentations generated.")

    def generate_problem_markdown(self, problem, path, has_get_code):
        text_path = os.path.join(path, "{:0>3d}-{}".format(problem['frontedId'], problem['slug']))
        if not os.path.isdir(text_path):
            os.mkdir(text_path)

        with open(os.path.join(text_path, "README.md"), 'w', encoding='utf-8') as f:
            f.write("# [{}][title]\n".format(problem['title']))
            f.write("\n## Description\n\n")

            content = html2text.html2text(problem['content'])
            content = content.replace("**Input:**", "Input:")
            content = content.replace("**Output:**", "Output:")
            content = content.replace('**Explanation:**', 'Explanation:')
            content = content.replace('\n    ', '    ')
            f.write(content)

            f.write("\n**Tags:** {}\n".format(problem['tags']))
            f.write("\n**Difficulty:** {}\n".format(problem['difficulty']))
            f.write("\n## Solution\n")

            if self.is_login and has_get_code:
                sql = "SELECT code, language FROM last_ac_submission_record WHERE problem_slug = ? ORDER BY timestamp"
                cursor = self.conn.cursor()
                cursor.execute(sql, (problem['slug'],))
                submission = cursor.fetchone()
                cursor.close()

                if submission is not None:
                    f.write("\n``` %s\n" % (submission[1]))
                    f.write(submission[0].encode('utf-8').decode('unicode_escape'))
                    f.write("\n```\n")

            f.write("\n[title]: https://leetcode.com/problems/{}\n".format(problem['slug']))

    def filter_problems(self, p_detail):
        if self.parameters.get('difficulty') and p_detail.get('difficulty') not in self.parameters['difficulty']:
            logging.debug(f"difficulty seems different")
            return False
        if self.parameters.get('status') and p_detail.get('status') not in self.parameters['status']:
            logging.debug(f"status seems different")
            return False

        tag_cursor = self.conn.cursor()
        tag_cursor.execute(
            'SELECT tag FROM problem_tags WHERE problem_id = ?', (p_detail['id'],))
        tag_list = tag_cursor.fetchall()
        tag_cursor.close()
        if self.parameters.get('tags'):
            tag_count = sum([1 for tag in tag_list if tag[0]
                             in self.parameters['tags']])
            if tag_count != len(self.parameters['tags']):
                return False
        return True

    def get_ac_problem_submission(self):
        if not self.is_login:
            logging.warning(
                f"account not given, so submission files cannot be fetched")
            return
        sql = "SELECT id,slug,difficulty,status FROM problems WHERE status = 'ac';"
        cursor = self.conn.cursor()
        cursor.execute(sql)
        results = cursor.fetchall()

        slug_list = []
        for row in results:
            question_detail = {
                'id': row[0],
                'slug': row[1],
                'difficulty': row[2],
                'status': row[3]
            }

            logging.debug(f"filtering problem, detail={question_detail}")
            if not self.filter_problems(question_detail):
                continue

            logging.debug(f"question_detail passed")

            slug = question_detail['slug']
            slug_list.append(question_detail['slug'])
            is_ok = False
            while not is_ok:
                try:
                    url = "https://leetcode.com/graphql"
                    params = {'operationName': "Submissions",
                              'variables': {"offset": 0, "limit": 20, "lastKey": '', "questionSlug": slug},
                              'query': '''query Submissions($offset: Int!, $limit: Int!, $lastKey: String, $questionSlug: String!) {
                                submissionList(offset: $offset, limit: $limit, lastKey: $lastKey, questionSlug: $questionSlug) {
                                lastKey
                                hasNext
                                submissions {
                                    id
                                    statusDisplay
                                    lang
                                    runtime
                                    timestamp
                                    url
                                    isPending
                                    __typename
                                }
                                __typename
                            }
                        }'''
                              }

                    json_data = json.dumps(params).encode('utf8')

                    headers = {'User-Agent': user_agent, 'Connection': 'keep-alive',
                               'Referer': 'https://leetcode.com/accounts/login/',
                               "Content-Type": "application/json", 'x-csrftoken': self.csrftoken}
                    resp = self.session.post(
                        url, data=json_data, headers=headers, timeout=10)
                    content = resp.json()

                    for submission in content['data']['submissionList']['submissions']:
                        if submission['statusDisplay'] == "Accepted":
                            cursor.execute(
                                "SELECT COUNT(*) FROM last_ac_submission_record WHERE id =" + str(submission['id']))
                            if cursor.fetchone()[0] == 0:
                                submission_get_success = False
                                while not submission_get_success:
                                    time.sleep(random.randint(1, 3))
                                    code_content = self.session.get("https://leetcode.com" + submission['url'],
                                                                    headers=headers, timeout=10)
                                    pattern = re.compile(
                                        r'submissionCode: \'(?P<code>.*)\',\n  editCodeUrl', re.S)
                                    m1 = pattern.search(code_content.text)
                                    src = m1.groupdict()['code'] if m1 else None
                                    if not src:
                                        logging.warning(f"Can not get [{slug}] solution code")
                                        continue
                                    submission_get_success = True

                                submission_detail = (submission['id'],
                                                     slug,
                                                     submission['timestamp'],
                                                     submission['lang'],
                                                     submission['runtime'],
                                                     src)
                                cursor.execute(
                                    "INSERT INTO last_ac_submission_record (id, problem_slug, timestamp, language, runtime, code) VALUES(?, ?, ?, ?, ?, ?)",
                                    submission_detail)
                                logging.info(
                                    "Insert submission[%s] success" % (submission['id']))
                                self.conn.commit()
                            is_ok = True
                            break
                except (requests.exceptions.Timeout, requests.exceptions.ConnectionError) as error:
                    logging.error(error)
                except json.decoder.JSONDecodeError as error:
                    logging.warning(error)
        cursor.close()

    def generate_problems_submission(self):
        if not self.is_login:
            logging.warning(
                f"account not given, so submission files cannot be fetched")
            return

        sql = """
            SELECT l.problem_slug, l.code,l.language, p.frontend_id, max(l.timestamp) FROM last_ac_submission_record as l, problems as p 
            WHERE l.problem_slug == p.slug and p.status = 'ac' GROUP BY l.problem_slug
        """
        cursor = self.conn.cursor()
        cursor.execute(sql)

        filter_cursor = self.conn.cursor()
        for submission in cursor:
            filter_cursor.execute(
                "SELECT id,slug,difficulty,status FROM problems WHERE slug = ?", (submission[0],))
            result = filter_cursor.fetchone()
            if not self.filter_problems({
                'id': result[0],
                'slug': result[1],
                'difficulty': result[2],
                'status': result[3]
            }):
                continue
            self.generate_problem_submission(
                self.parameters['directory'], submission)

        cursor.close()
        filter_cursor.close()

    def generate_problem_submission(self, path, submission):
        if not os.path.isdir(path):
            os.mkdir(path)

        text_path = os.path.join(
            path, "{:0>3d}-{}".format(submission[3], submission[0]))

        logging.debug(f"generate submission, path = {path}, text_path = {text_path}")
        if not os.path.isdir(text_path):
            os.mkdir(text_path)
        with open(os.path.join(text_path, "solution.txt"), 'w', encoding='utf-8') as f:
            f.write(submission[1].encode('utf-8').decode('unicode_escape'))

    def close_db(self):
        self.conn.close()


if __name__ == '__main__':
    argsDict = arg_parser(sys.argv)
    crawler = LeetcodeCrawler()
    crawler.init(argsDict)

    crawler.connect_db(db_path)
    crawler.get_problems()

    if argsDict.get('code'):
        crawler.get_ac_problem_submission()
        crawler.generate_problems_submission()

    crawler.generate_problems_markdown()
    crawler.generate_problems_excel()

    crawler.close_db()
